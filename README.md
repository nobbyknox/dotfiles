# DOTFILES

```
      :::::::::  :::::::::::::::::::::::::::::::::::::::::::       :::::::::::::::::: 
     :+:    :+::+:    :+:   :+:    :+:           :+:    :+:       :+:      :+:    :+: 
    +:+    +:++:+    +:+   +:+    +:+           +:+    +:+       +:+      +:+         
   +#+    +:++#+    +:+   +#+    :#::+::#      +#+    +#+       +#++:++# +#++:++#++   
  +#+    +#++#+    +#+   +#+    +#+           +#+    +#+       +#+             +#+    
 #+#    #+##+#    #+#   #+#    #+#           #+#    #+#       #+#      #+#    #+#     
#########  ########    ###    ###       #######################################       
```

## Neovim

On Archlinux you might want to install the following in order to support the
requirements for my Neovim setup:

```bash
# pacman -S git mc tmux neovim nodejs npm community/python-pynvim ripgrep community/powerline-fonts community/ttf-nerd-fonts-symbols-mono
```

