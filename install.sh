#!/bin/bash
#
# https://www.gnu.org/software/bash/manual/bash.html
# https://man7.org/linux/man-pages/man1/bash.1.html
# https://devhints.io/bash

NVIM_CONFIG_DIR="$HOME/.config/nvim"
TMUX_CONFIG_FILE="$HOME/.tmux.conf"
GIT_CONFIG_FILE="$HOME/.gitconfig"

banner() {
    term_cols=$(tput cols)
    str=":: $1 ::"
    for ((i=1; i<=$term_cols; i++)); do echo -n -; done
    tput setaf 10; printf "%*s\n" $(((${#str}+$term_cols)/2)) "$str"; tput sgr0
    for ((i=1; i<=$term_cols; i++)); do echo -n -; done
    printf "\n"
}

install_nvim() {
    echo "Installing Neovim config into $NVIM_CONFIG_DIR ..."

    local nvim_file="init.vim"
    local plugins_dir="plugins"
    local backup_dir="$HOME/.local/share/nvim/backup"
    # .local/share/nvim/backup

    # Check for the base target directory
    if [[ ! -d "$NVIM_CONFIG_DIR" ]]; then
        mkdir -p $NVIM_CONFIG_DIR
        echo "Directory $NVIM_CONFIG_DIR created"
    fi

    # Check for the plugins directory
    if [[ ! -d "$NVIM_CONFIG_DIR/$plugins_dir" ]]; then
        mkdir -p $NVIM_CONFIG_DIR/$plugins_dir
        echo "Directory $NVIM_CONFIG_DIR/$plugins_dir created"
    fi

    # Symlink the init.vim file
    if [[ ! -h "$NVIM_CONFIG_DIR/$nvim_file" ]]; then
        ln -s $(realpath nvim/$nvim_file) $NVIM_CONFIG_DIR/$nvim_file
        echo "Symlink created: $NVIM_CONFIG_DIR/$nvim_file"
    fi

    # Symlink the plugins
    for file in $(find nvim/$plugins_dir -name *.vim)
    do
        tmp_file=$(basename $file)

        if [[ ! -h "$NVIM_CONFIG_DIR/$plugins_dir/$tmp_file" ]]; then
            ln -s $(realpath nvim/$plugins_dir/$tmp_file) $NVIM_CONFIG_DIR/$plugins_dir/$tmp_file
            echo "Symlink created: $NVIM_CONFIG_DIR/$plugins_dir/$tmp_file"
        fi
    done

    if [[ ! -d "$backup_dir" ]]; then
        mkdir -p $backup_dir
        echo "Directory $backup_dir created"
    fi

    echo "Neovim configuration installation complete"
    echo "NOTE: Remember to install the plugins by running ':PlugInstall' in Neovim"

}

install_tmux() {
    echo "Installing tmux config ..."

    # Symlink the .tmux.conf file
    if [[ ! -h "$TMUX_CONFIG_FILE" ]]; then
        ln -s $(realpath tmux/.tmux.conf) $TMUX_CONFIG_FILE
        echo "Symlink created: $TMUX_CONFIG_FILE"
    fi
}

install_git() {
    echo "Installing git config ..."

    # Symlink the .gitconfig file
    if [[ ! -h "$GIT_CONFIG_FILE" ]]; then
        ln -s $(realpath git/.gitconfig) $GIT_CONFIG_FILE
        echo "Symlink created: $GIT_CONFIG_FILE"
    fi
}

################################################################################
# Main block
################################################################################

banner "Dotfile Installer"

install_nvim
install_tmux
install_git

echo "Dotfile installation complete"
