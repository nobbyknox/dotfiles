# nvim

## Status

| Date       | Comment |
| :--------- | :------ |
| 2021-11-26 | Created |

## Introduction

Inspiration for this configuration came from https://github.com/jessarcher/dotfiles

## Installation

* Use the `install.sh` script
* Install plugins with:
    ```bash
    nvim +PlugInstall
    ```

