#!/bin/bash

export PATH=$PATH:/Users/nobby/Apps/flutter-3.22.2/bin
export ALMOST_TIME_ENV_SCRIPT=/Users/nobby/dev/almost-time/compose-env/nobby-env-dev.sh
export ALMOST_TIME_ENV_SCRIPT_UNIT_TEST=/Users/nobby/dev/almost-time/compose-env/nobby-env-unit-test.sh
