# Mac Setup

2024-07-10

Perform OS upgrade

## Install Homebrew:

```shell
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
$ (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/nobby/.zsh
$ eval "$(/opt/homebrew/bin/brew shellenv)"
$ brew update
```

## Install a Few Things

```shell
$ brew install mc htop curl wget cat uname find fzf zoxide
$ brew install --cask zed rectangle kitty vivaldi beekeeper-studio vlc
```

## Install CopyQ

```shell
$ brew install copyq
```

You might "the app is damaged". See below for info and how to fix it.

https://github.com/hluk/CopyQ/issues/2652

TL;DR: Do the following:

```shell
$ xattr -rd com.apple.quarantine /Applications/CopyQ.app
$ codesign -f --deep -s - /Applications/CopyQ.app
```

## Install Mos

```shell
$ brew install mos
```

Fix it with:

```shell
$ xattr -rd com.apple.quarantine /Applications/Mos.app
```

## Install Fonts

```shell
$ brew install font-meslo-lg-nerd-font font-fira-code
```

Set font in `~/.config/kitty/kitty.conf` to `MesloLGS Nerd Font Mono` and font size to 16.0.

## Install Terminal Theme

See https://www.josean.com/posts/how-to-setup-alacritty-terminal

```shell
$ brew install powerlevel10k
$ echo "source $(brew --prefix)/share/powerlevel10k/powerlevel10k.zsh-theme" >>~/.zshrc
```

To configure later, enter:

```shell
$ p10k configure
```

## Install Jetbrains Stuff

```shell
$ brew install font-jetbrains-mono jetbrains-toolbox
```

See Bitwarden for login credentials.

## Install Visual Studio Code

```shell
$ brew install visual-studio-code
```

Add these plugins:

* EditorConfig for VS Code

## Install Eza (better ls)

https://eza.rocks

```shell
$ brew install eza
```

Add these aliases to `~/.zshrc`:

```shell
alias ll='eza -l --icons=never'
alias la='eza -la --icons=never'
alias lt='eza -l --tree'
alias lat='eza -la --tree'
```

## Install LazyVim

https://www.lazyvim.org

```shell
$ git clone https://github.com/LazyVim/starter ~/.config/nvim
$ rm -rf ~/.config/nvim/.git
$ brew install neovim
```

Run `nvim` to install all plugins.

## Install Tmux

```shell
$ brew install tmux
```

TODO: Configure + themes

## SDKMan

TODO: Complete this

```shell
$ curl -s "https://get.sdkman.io" | bash
```

## NVM (Node Version Manager)

https://github.com/nvm-sh/nvm

```shell
$ brew install nvm
```

Then copy the following to the bottom of `~/.zshrc`:

```
export NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
# [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
```

## Configure Git

Copy the following to `~/.gitconfig`:

// TODO: Complete this

## Install Z

```shell
$ brew install z
```

Add the following line to `~/.zshrc`:

```
. /opt/homebrew/etc/profile.d/z.sh
```

## Rosetta

Rosetta is needed by Flutter.

```shell
$ sudo softwareupdate --install-rosetta --agree-to-license
```

## Docker

```shell
$ brew install --cask docker
```

This will also install `compose`.
