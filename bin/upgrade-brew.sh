#!/bin/bash

brew update
brew upgrade
brew upgrade --cask beekeeper-studio font-meslo-lg-nerd-font mos vivaldi copyq jetbrains-toolbox rectangle zed font-jetbrains-mono kitty visual-studio-code brave-browser ghostty
